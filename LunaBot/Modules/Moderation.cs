﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Discord;
using Discord.Commands;
using Discord.WebSocket;

namespace LunaBot
{
    [Remarks("3")]
    public class Moderation : ModuleBase
    {
        private IServiceProvider map;

        public Moderation(IServiceProvider map)
        {
            this.map = map;
        }

        [Command("sudo")]
        [Summary("Execute command as another user")]
        [RequireOwner]
        public async Task Sudo(
            [Summary("Execute as")] IGuildUser user, 
            [Summary("Command to execute"), Remainder] string command)
        {
            CommandService service = map.GetService<CommandService>();
            DiscordSocketClient client = map.GetService<DiscordSocketClient>();

            //Context.Message.Author = user;

            var ctx = new CommandContext(client, Context.Message);
            await service.ExecuteAsync(ctx, command);
        }

        [Command("purge")]
        [Summary("Purge last 100 or specified number of messages")]
        [RequireUserPermission(GuildPermission.ManageMessages)]
        [RequireBotPermission(GuildPermission.ManageMessages)]
        public async Task Purge(
            [Summary("Number of messages to purge (Max: 100)")] int purgeAmt = 100)
        {
            purgeAmt = Math.Min(100, purgeAmt);
            var messages = await Context.Channel.GetMessagesAsync(purgeAmt).Flatten();
            await Context.Channel.DeleteMessagesAsync(messages);
            await ReplyAsync($"`{Context.User.Username} deleted {purgeAmt} messages from the channel.`");
        }

        [Command("kick")]
        [Summary("Kick user from the server")]
        public async Task Kick(
            [Summary("User to kick")] IGuildUser user, 
            [Summary("Reason for kick"), Remainder] string reason)
        {
            await user.KickAsync(reason);

            GuildConfig config = GuildConfig.Load(Context.Guild);

            ModActionsModel action = new ModActionsModel()
            {
                ModActionID = config.ModActionID++,
                UserID = user.Id,
                ModOperation = (int)LunaUtil.modOperations.Kick,
                IssueTime = DateTime.Now,
            };

            if (config.LogLevel > 0)
            {
                ITextChannel logChannel = await Context.Guild.GetTextChannelAsync(config.LogChannel);
                await logChannel.SendMessageAsync("", embed: LunaUtil.ModLog(action, user, Context.User as IGuildUser, reason));
            }

            await LunaUtil.CommandSuccess(Context.Message);
        }

        [Command("mute")]
        [Summary("Server mute a user")]
        public async Task Mute(
            [Summary("User to mute")] IGuildUser user, 
            [Summary("Duration in minutes")] int duration,
            [Summary("Reason for mute"), Remainder] string reason)
        {
            await user.ModifyAsync(x => x.Mute = true);

            GuildConfig config = GuildConfig.Load(Context.Guild);

            ModActionsModel action = new ModActionsModel()
            {
                ModActionID = config.ModActionID++,
                UserID = user.Id,
                ModOperation = (int)LunaUtil.modOperations.Mute,
                IssueTime = DateTime.Now,
                Duration = duration
            };

            config.ModActions.Add(action);
            config.Save();

            if (config.LogLevel > 0)
            {
                ITextChannel logChannel = await Context.Guild.GetTextChannelAsync(config.LogChannel);
                await logChannel.SendMessageAsync("", embed: LunaUtil.ModLog(action, user, Context.User as IGuildUser, reason));
            }

            await LunaUtil.CommandSuccess(Context.Message);
        }

        [Command("textmute")]
        [Summary("Text mute a user on all channels")]
        public async Task TextMute(
            [Summary("User to textmute")] IGuildUser user, 
            [Summary("Duration in minutes")] int duration, 
            [Summary("Reason for textmute"), Remainder] string reason)
        {
            GuildConfig config = GuildConfig.Load(Context.Guild);
            List<ITextChannel> channels = (await Context.Guild.GetTextChannelsAsync()).ToList();
            
            foreach (ITextChannel channel in channels)
            {
                OverwritePermissions mutePerm = OverwritePermissions.InheritAll;
                mutePerm = mutePerm.Modify(sendMessages: PermValue.Deny);
                await channel.AddPermissionOverwriteAsync(user, mutePerm);
            }

            ModActionsModel action = new ModActionsModel()
            {
                ModActionID = config.ModActionID++,
                UserID = user.Id,
                ModOperation = (int)LunaUtil.modOperations.TextMute,
                IssueTime = DateTime.Now,
                Duration = duration,
                Channels = channels.Select(x => x.Id).ToList()
            };

            config.ModActions.Add(action);
            config.Save();

            if (config.LogLevel > 0)
            {
                ITextChannel logChannel = await Context.Guild.GetTextChannelAsync(config.LogChannel);
                await logChannel.SendMessageAsync("", embed: LunaUtil.ModLog(action, user, Context.User as IGuildUser, reason));
            }
        }

        [Command("ban")]
        [Summary("Ban a user from the server")]
        public async Task Ban(
            [Summary("User to ban")] IGuildUser user, 
            [Summary("Duration in minutes")] int duration, 
            [Summary("Reason for ban"), Remainder] string reason)
        {
            await Context.Guild.AddBanAsync(user, reason: reason);

            GuildConfig config = GuildConfig.Load(Context.Guild);

            ModActionsModel action = new ModActionsModel()
            {
                ModActionID = config.ModActionID++,
                UserID = user.Id,
                ModOperation = (int)LunaUtil.modOperations.Ban,
                IssueTime = DateTime.Now,
                Duration = duration
            };

            config.ModActions.Add(action);
            config.Save();

            if (config.LogLevel > 0)
            {
                ITextChannel logChannel = await Context.Guild.GetTextChannelAsync(config.LogChannel);
                await logChannel.SendMessageAsync("", embed: LunaUtil.ModLog(action, user, Context.User as IGuildUser, reason));
            }

            await LunaUtil.CommandSuccess(Context.Message);
        }

        [Command("softban")]
        [Summary("Kick user and remove past messages")]
        public async Task SoftBan(
            [Summary("User to softban")] IGuildUser user, 
            [Summary("Number of days of messages to remove (0-7)")] int daysToRemove, 
            [Summary("Reason for softban"), Remainder] string reason)
        {
            if (daysToRemove < 0) daysToRemove = 0;
            else if (daysToRemove > 7) daysToRemove = 7;

            await Context.Guild.AddBanAsync(user, daysToRemove, reason);
            await Context.Guild.RemoveBanAsync(user);

            GuildConfig config = GuildConfig.Load(Context.Guild);

            ModActionsModel action = new ModActionsModel()
            {
                ModActionID = config.ModActionID++,
                UserID = user.Id,
                ModOperation = (int)LunaUtil.modOperations.SoftBan,
                IssueTime = DateTime.Now,
            };

            if (config.LogLevel > 0)
            {
                ITextChannel logChannel = await Context.Guild.GetTextChannelAsync(config.LogChannel);
                await logChannel.SendMessageAsync("", embed: LunaUtil.ModLog(action, user, Context.User as IGuildUser, reason));
            }

            await LunaUtil.CommandSuccess(Context.Message);
        }

        [Command("moveall")]
        [Summary("Move all users from current channel to another")]
        public async Task MoveAll(
            [Summary("EXACT channel name or ID"), Remainder] string channelName)
        {

        }

        [Command("moveall")]
        [Summary("Move all users from specified channel to another")]
        public async Task MoveAll(
            [Summary("EXACT channel name or ID")] string currentChannel,
            [Summary("EXACT channel name or ID")] string destinationChannel)
        {

        }
    }
}

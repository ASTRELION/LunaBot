﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Discord.Addons.Interactive;
using Microsoft.Extensions.DependencyInjection;

namespace LunaBot
{
    [Remarks("1")]
    public class General : ModuleBase
    {
        private IServiceProvider map;

        public General(IServiceProvider map)
        {
            this.map = map;
        }

        [Group("Channel")]
        [Alias("PrivateChannel")]
        public class Channel : ModuleBase
        {
            [Command("create")]
            [Summary("Create a private (enter PUBLIC first for public channel) channel with given name")]
            public async Task Create(
                [Summary("Channel name"), Remainder] string channelName)
            {
                GuildConfig config = GuildConfig.Load(Context.Guild);

                if (!HasChannel(config, Context.User))
                {
                    IGuild guild = Context.Guild;
                    string name = channelName;
                    
                    IVoiceChannel channel;

                    var input = channelName.Split(' ');
                    if (input[0] != "PUBLIC")
                    {
                        channel = await guild.CreateVoiceChannelAsync(name);

                        OverwritePermissions ownerPerms = OverwritePermissions.InheritAll;
                        ownerPerms = ownerPerms.Modify
                        (
                            connect: PermValue.Allow,
                            manageChannel: PermValue.Allow,
                            managePermissions: PermValue.Allow
                        );

                        OverwritePermissions everyonePerms = OverwritePermissions.InheritAll;
                        everyonePerms = everyonePerms.Modify(connect: PermValue.Deny);

                        await channel.AddPermissionOverwriteAsync(Context.User, permissions: ownerPerms);
                        await channel.AddPermissionOverwriteAsync(Context.Guild.EveryoneRole, permissions: everyonePerms);
                    }
                    else
                    {
                        name = string.Join(" ", input.Skip(1));
                        channel = await guild.CreateVoiceChannelAsync(name);

                        OverwritePermissions ownerPerms = OverwritePermissions.InheritAll;
                        ownerPerms = ownerPerms.Modify
                        (
                            connect: PermValue.Allow,
                            manageChannel: PermValue.Allow,
                            managePermissions: PermValue.Allow
                        );

                        await channel.AddPermissionOverwriteAsync(Context.User, permissions: ownerPerms);
                    }

                    config.PrivateChannels.Add(new PrivateChannelModel()
                    {
                        ChannelID = channel.Id,
                        OwnerID = Context.User.Id,
                        TimeSinceEmpy = DateTime.Now.AddMinutes(30)
                    });
                    config.Save();

                    await LunaUtil.CommandSuccess(Context.Message);
                }
                else
                {
                    await ReplyAsync("You already have a channel.");
                    await LunaUtil.CommandFail(Context.Message);
                } 
            }

            [Command("add")]
            [Summary("Add user(s) to your existing private channel")]
            public async Task Add(
                [Summary("User(s) to add")] params IGuildUser[] users)
            {
                GuildConfig config = GuildConfig.Load(Context.Guild);
                Console.WriteLine(users.Length);
                if (HasChannel(config, Context.User))
                {
                    PrivateChannelModel channelModel = config.PrivateChannels.Where(x => x.OwnerID == Context.User.Id).First();
                    IVoiceChannel channel = await Context.Guild.GetVoiceChannelAsync(channelModel.ChannelID);

                    OverwritePermissions addPerms = OverwritePermissions.InheritAll;
                    addPerms = addPerms.Modify(connect: PermValue.Allow);
                    
                    foreach (IGuildUser user in users)
                    {
                        await channel.AddPermissionOverwriteAsync(user, permissions: addPerms);
                    }

                    await LunaUtil.CommandSuccess(Context.Message);
                }
                else
                {
                    await ReplyAsync("You don't have a channel.");
                    await LunaUtil.CommandFail(Context.Message);
                }
            }

            [Command("remove")]
            [Summary("Remove user(s) from your existing private channel")]
            public async Task Remove(
                [Summary("User(s) to remove")] params IGuildUser[] users)
            {
                GuildConfig config = GuildConfig.Load(Context.Guild);

                if (HasChannel(config, Context.User))
                {
                    PrivateChannelModel channelModel = config.PrivateChannels.Where(x => x.OwnerID == Context.User.Id).First();
                    IVoiceChannel channel = await Context.Guild.GetVoiceChannelAsync(channelModel.ChannelID);

                    foreach (IGuildUser user in users)
                    {
                        await channel.RemovePermissionOverwriteAsync(user);
                    }

                    await LunaUtil.CommandSuccess(Context.Message);
                }
                else
                {
                    await ReplyAsync("You don't have a channel.");
                    await LunaUtil.CommandFail(Context.Message);
                }
            }

            [Command("public")]
            [Summary("Set your channel as public t/f")]
            public async Task Public(
                [Summary("Is public")] Boolean isPublic)
            {
                GuildConfig config = GuildConfig.Load(Context.Guild);

                if (HasChannel(config, Context.User))
                {
                    PrivateChannelModel channelModel = config.PrivateChannels.Where(x => x.OwnerID == Context.User.Id).First();
                    IVoiceChannel channel = await Context.Guild.GetVoiceChannelAsync(channelModel.ChannelID);

                    if (isPublic)
                    {
                        foreach (Overwrite role in channel.PermissionOverwrites)
                        {
                            if (role.TargetId != channelModel.OwnerID)
                            {
                                await channel.RemovePermissionOverwriteAsync(Context.Guild.GetRole(role.TargetId));
                            }
                        }

                        OverwritePermissions everyonePerms = OverwritePermissions.InheritAll;
                        everyonePerms = everyonePerms.Modify(connect: PermValue.Inherit);

                        await channel.AddPermissionOverwriteAsync(Context.Guild.EveryoneRole, everyonePerms);
                    }
                    else
                    {
                        OverwritePermissions everyonePerms = OverwritePermissions.InheritAll;
                        everyonePerms = everyonePerms.Modify(connect: PermValue.Deny);

                        await channel.AddPermissionOverwriteAsync(Context.Guild.EveryoneRole, everyonePerms);
                    }

                    await LunaUtil.CommandSuccess(Context.Message);
                }
                else
                {
                    await ReplyAsync("You don't have a channel.");
                    await LunaUtil.CommandFail(Context.Message);
                }
            }

            [Command("delete")]
            [Summary("Delete your channel")]
            public async Task Delete()
            {
                GuildConfig config = GuildConfig.Load(Context.Guild);

                if (HasChannel(config, Context.User))
                {
                    PrivateChannelModel channelModel = config.PrivateChannels.Where(x => x.OwnerID == Context.User.Id).First();

                    await (await Context.Guild.GetVoiceChannelAsync(channelModel.ChannelID)).DeleteAsync();
                    config.PrivateChannels.Remove(channelModel);
                    config.Save();

                    await LunaUtil.CommandSuccess(Context.Message);
                }
                else
                {
                    await ReplyAsync("You don't have a channel.");
                    await LunaUtil.CommandFail(Context.Message);
                }
            }

            public Boolean HasChannel(GuildConfig config, IUser user)
            {
                return config.PrivateChannels.Where(x => x.OwnerID == user.Id).FirstOrDefault() != null;
            }
        }

        [Command("whosplaying")]
        [Summary("Get a list of players playing given game")]
        public async Task WhoPlaying(
            [Summary("Name of the game"), Remainder] string gameName)
        {
            var users = (await Context.Guild.GetUsersAsync()).Where(x => x.Game.ToString().ToLower().Contains(gameName.ToLower()));

            EmbedBuilder embed = new EmbedBuilder()
            {
                Title = $"Users playing...",
                Color = LunaUtil.offWhite
            };

            foreach (IGuildUser u in users)
            {
                var gameField = embed.Fields.Where(x => x.Name.ToLower().Equals(u.Game.ToString().ToLower())).FirstOrDefault();
                if (gameField == null)
                {
                    EmbedFieldBuilder newGame = new EmbedFieldBuilder()
                    {
                        Name = u.Game.ToString() + "\u200B",
                        Value = $"{u.Username}\n"
                    };

                    embed.AddField(newGame);
                }
                else
                {
                    gameField.Value += $"{u.Username}\n";
                }
            }

            if (embed.Fields.Count == 0)
            {
                embed.Description = $"No users found playing '{gameName}'";
            }

            await ReplyAsync("", embed: embed);
        }

        [Command("serverinfo")]
        [Summary("Get information about the current server")]
        public async Task ServerInfo()
        {
            EmbedBuilder e = new EmbedBuilder()
            {
                Title = $"{Context.Guild.Name} Information",
                Description = $"Name: {Context.Guild.Name}" +
                              $"\nOwner: {Context.Guild.GetOwnerAsync().Result.Username}" +
                              $"\n# of Members: {Context.Guild.GetUsersAsync().Result.Count}" +
                              $"\nDate Created: {Context.Guild.CreatedAt.Date.ToShortDateString()}" +
                              $"\nAge: {(DateTime.Now - Context.Guild.CreatedAt).Days} days" +
                              $"\nSecurity: {Context.Guild.VerificationLevel}",
                ThumbnailUrl = Context.Guild.IconUrl,
                Color = LunaUtil.offWhite
            };

            var messages = await Context.Channel.GetMessagesAsync(100).Flatten();
            double image = 0, text = 0, link = 0, command = 0;
            LunaConfig config = LunaConfig.Load();

            foreach (var m in messages)
            {
                if (m.Attachments.Count > 0)
                    image++;

                if (m.Content.Contains("http://") || m.Content.Contains("https://"))
                    link++;

                if (m.Content.Length > 0)
                    text++;

                if (m.Content.StartsWith(config.Prefix.ToString()))
                    command++;
            }

            var voiceChannels = await Context.Guild.GetVoiceChannelsAsync();
            var textChannels = await Context.Guild.GetTextChannelsAsync();

            var roles = Context.Guild.Roles;

            EmbedFieldBuilder channelData = new EmbedFieldBuilder()
            {
                Name = "Channel Data",
                Value = $"# of Text Channels: {textChannels.Count}" +
                        $"\n# of Voice Channels: {voiceChannels.Count}"
            };

            EmbedFieldBuilder roleData = new EmbedFieldBuilder()
            {
                Name = "Role Data",
                Value = $"# of Roles: {roles.Count}"
            };

            EmbedFieldBuilder messageData = new EmbedFieldBuilder()
            {
                Name = "Message Data (prev. 100)",
                Value = $"Images: {image}%" +
                        $"\nText: {text}%" +
                        $"\nLink: {link}%" +
                        $"\nCommand: {command}%"
            };

            e.AddField(channelData);
            e.AddField(roleData);
            e.AddField(messageData);

            await ReplyAsync("", embed: e.Build());
        }

        [Command("botinfo")]
        [Summary("Get information about the Luna bot")]
        public async Task BotInfo()
        {
            var client = map.GetService<DiscordSocketClient>();
            IApplication bot = await client.GetApplicationInfoAsync();
            
            EmbedBuilder e = new EmbedBuilder()
            {
                Title = $"{bot.Name} Information",
                Description = $"Name: {bot.Name}" +
                              $"\nAuthor: {bot.Owner.Username}" +
                              $"\nDescription: {bot.Description}" +
                              $"\nDate Created: {bot.CreatedAt}" +
                              $"\nAge: {(DateTime.Now - bot.CreatedAt).Days} days" +
                              $"\nGuilds Joined: {client.Guilds.Count}",
                ThumbnailUrl = bot.IconUrl,
                Color = LunaUtil.offWhite
            };

            await ReplyAsync("", embed: e);
        }

        // !ping
        [Command("ping")]
        [Summary("Ping the bot")]
        public async Task Ping()
        {
            DiscordSocketClient client = map.GetService<DiscordSocketClient>();
            var m = await ReplyAsync($"Pong! {client.Latency}ms");
        }

        // !help
        [Command("help")]
        [Summary("List all commands you have access to")]
        [Alias("h","?")]
        public async Task Help()
        {
            List<ModuleInfo> moduleList = map.GetService<List<ModuleInfo>>();
            
            var message = await ReplyAsync(Context.User.Mention, embed: LunaUtil.GetHelpPage(1, moduleList, Context as CommandContext));
            
            for (int i = 0; i < moduleList.Count; i++)
            {
                await message.AddReactionAsync(new Emoji((i + 1) + "\u20e3"));
            }
        }

        [Command("help")]
        [Summary("Get help on a specific command")]
        [Alias("h", "?")]
        public async Task Help(
            [Summary("Command to get help with"), Remainder] string command)
        {
            CommandService commands = map.GetService<CommandService>();

            try
            {
                var cmds = commands.Commands.Where(x => x.Name.ToUpper().Equals(command.ToUpper())).ToList();
                
                foreach (var cmd in cmds)
                {
                    EmbedBuilder e = new EmbedBuilder()
                    {
                        Title = $"**!{cmd.Name} Help**",
                        Description = $"{cmd.Summary}",
                        Color = LunaUtil.offWhite
                    };

                    EmbedFieldBuilder syntax = new EmbedFieldBuilder()
                    {
                        Name = "Command Syntax",
                        Value = $"{LunaUtil.GetCommandSyntax(cmd)}\t\t\u200B",
                        IsInline = true
                    };

                    EmbedFieldBuilder alias = new EmbedFieldBuilder()
                    {
                        Name = "Command Alias(es)",
                        Value = "\u200B",
                        IsInline = true
                    };

                    for (int i = 0; i < cmd.Aliases.Count; i++)
                    {
                        alias.Value += $"!{cmd.Aliases[i]}" + (i == cmd.Aliases.Count - 1 ? "" : ", ");
                    }

                    e.AddField(syntax);
                    e.AddField(alias);

                    if (cmd.Parameters.Count > 0)
                    {
                        EmbedFieldBuilder param = new EmbedFieldBuilder()
                        {
                            Name = "\u200B",
                            Value = "**Command Parameter(s)**",
                            IsInline = false
                        };

                        e.AddField(param);

                        foreach (var p in cmd.Parameters)
                        {
                            EmbedFieldBuilder par = new EmbedFieldBuilder()
                            {
                                Name = p.Name,
                                Value = $"{p.Summary}" +
                                        $"\nRequired? {!p.IsOptional}",
                                IsInline = true
                            };

                            e.AddField(par);
                        }
                    }

                    await ReplyAsync("", embed: e);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        [Command("moveto")]
        [Summary("Move yourself to a different voice channel")]
        public async Task MoveTo(
            [Summary("EXACT channel name or id"), Remainder] string channelName)
        {
            IGuildUser user = Context.User as IGuildUser;
            IVoiceChannel channel;

            if (ulong.TryParse(channelName, out ulong channelID))
                channel = await Context.Guild.GetVoiceChannelAsync(channelID);
            else
                channel = (await Context.Guild.GetVoiceChannelsAsync()).Where(x => x.Name.ToLower() == channelName.ToLower()).FirstOrDefault();

            if (channel != null)
            {
                if (user.GetPermissions(channel).Connect)
                {
                    await user.ModifyAsync(x => x.ChannelId = channel.Id);
                }  
                else
                {
                    await ReplyAsync($"You do not have permission to connect to {channel.Name}");
                    await LunaUtil.CommandFail(Context.Message);
                }
            }
            else
            {
                await ReplyAsync("Invalid channel name.");
                await LunaUtil.CommandFail(Context.Message);
            }
        }
    }
}
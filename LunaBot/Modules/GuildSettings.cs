﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;

namespace LunaBot.Modules
{
    [Remarks("4")]
    [Group("Settings")]
    public class GuildSettings : ModuleBase
    {
        [Command("roleonjoin")]
        [Summary("Role to give users on server join")]
        public async Task RoleOnJoin(
            [Summary("EXACT role name or role ID")] string roleName)
        {
            GuildConfig config = GuildConfig.Load(Context.Guild);

            IRole role;

            if (ulong.TryParse(roleName, out ulong roleID))
                role = Context.Guild.Roles.Where(x => x.Id == roleID).FirstOrDefault();
            else
                role = Context.Guild.Roles.Where(x => x.Name.ToLower() == roleName.ToLower()).FirstOrDefault();

            if (role != null)
            {
                config.RoleOnJoin = role.Id;
                config.Save();
                await LunaUtil.CommandSuccess(Context.Message);
            }
            else
            {
                await ReplyAsync("Role not found.");
                await LunaUtil.CommandFail(Context.Message);
            }
        }

        [Command("defaultChannel")]
        [Summary("Default channel to send greeting and afk messages to")]
        public async Task DefaultChannel(
            [Summary("EXACT channel name or ID"), Remainder] string channelName)
        {
            GuildConfig config = GuildConfig.Load(Context.Guild);

            ITextChannel channel;

            if (ulong.TryParse(channelName, out ulong channelID))
                channel = (await Context.Guild.GetTextChannelsAsync()).Where(x => x.Id == channelID).FirstOrDefault();
            else
                channel = (await Context.Guild.GetTextChannelsAsync()).Where(x => x.Name.ToLower() == channelName.ToLower()).FirstOrDefault();

            if (channel != null)
            {
                config.DefaultChannel = channel.Id;
                config.Save();
                await LunaUtil.CommandSuccess(Context.Message);
            }
            else
            {
                await ReplyAsync("Channel not found.");
                await LunaUtil.CommandFail(Context.Message);
            }
        }

        [Command("logchannel")]
        [Summary("Log channel to send log messages to")]
        public async Task LogChannel(
            [Summary("EXACT channel name or iD"), Remainder] string channelName)
        {
            GuildConfig config = GuildConfig.Load(Context.Guild);

            ITextChannel channel;

            if (ulong.TryParse(channelName, out ulong channelID))
                channel = (await Context.Guild.GetTextChannelsAsync()).Where(x => x.Id == channelID).FirstOrDefault();
            else
                channel = (await Context.Guild.GetTextChannelsAsync()).Where(x => x.Name.ToLower() == channelName.ToLower()).FirstOrDefault();

            if (channel != null)
            {
                config.LogChannel = channel.Id;
                config.Save();
                await LunaUtil.CommandSuccess(Context.Message);
            }
            else
            {
                await ReplyAsync("Channel not found.");
                await LunaUtil.CommandFail(Context.Message);
            }
        }

        [Command("loglevel")]
        [Summary("Set log level. 0:None 1:Mod 2:All")]
        public async Task LogLevel(
            [Summary("Log level 0-2")] int level)
        {
            if (level >= 0 && level <= 2)
            {
                GuildConfig config = GuildConfig.Load(Context.Guild);
                config.LogLevel = level;
                config.Save();

                await LunaUtil.CommandSuccess(Context.Message);
            }
            else
            {
                await ReplyAsync("Invalid log level");
                await LunaUtil.CommandFail(Context.Message);
            }
            
        }

        [Command("greeting")]
        [Summary("Display a greeting when a user joins the server for the first time. Use {USER} to replace users name.")]
        public async Task Greeting(
            [Summary("Greeting to display"), Remainder] string greeting)
        {
            GuildConfig config = GuildConfig.Load(Context.Guild);
            config.Greeting = greeting;
            config.Save();

            await LunaUtil.CommandSuccess(Context.Message);
        }

        [Command("farewell")]
        [Summary("Display a greeting when a user joins the server for the first time. Use {USER} to replace users name.")]
        public async Task Farewell(
            [Summary("Greeting to display"), Remainder] string farewell)
        {
            GuildConfig config = GuildConfig.Load(Context.Guild);
            config.Farewell = farewell;
            config.Save();

            await LunaUtil.CommandSuccess(Context.Message);
        }
    }
}

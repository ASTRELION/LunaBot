﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;
using Discord.Addons.Interactive;

namespace LunaBot
{
    [Remarks("2")]
    public class Games : ModuleBase
    {
        // Connect Four
        [Group("ConnectFour")]
        public class ConnectFour : ModuleBase
        {
            [Command("start")]
            [Summary("Start a connect four game with specified opponent")]
            public async Task Start(
                [Summary("Opponent to face")] IGuildUser opponent)
            {
                var g = RunningGames.ConnectFourGames.Values.Where(x => (ulong)x[0] == Context.User.Id).FirstOrDefault();

                if (g == null)
                {
                    string[,] boardList = new string[6, 7]; // r c

                    for (int r = 0; r <= boardList.GetUpperBound(0); r++)
                    {
                        for (int c = 0; c <= boardList.GetUpperBound(1); c++)
                        {
                            boardList[r, c] = "        ";
                            Console.Write(boardList[r, c]);
                        }
                        Console.WriteLine("");
                    }

                    // Build message
                    EmbedBuilder e = new EmbedBuilder()
                    {
                        Title = "Connect Four",
                        Description = "Loading Gameboard..."
                    };

                    // Display gameboard
                    var boardMsg = await ReplyAsync("", embed: e);
                    await boardMsg.AddReactionAsync(new Emoji("0\u20e3"));
                    await boardMsg.AddReactionAsync(new Emoji("1\u20e3"));
                    await boardMsg.AddReactionAsync(new Emoji("2\u20e3"));
                    await boardMsg.AddReactionAsync(new Emoji("3\u20e3"));
                    await boardMsg.AddReactionAsync(new Emoji("4\u20e3"));
                    await boardMsg.AddReactionAsync(new Emoji("5\u20e3"));
                    await boardMsg.AddReactionAsync(new Emoji("6\u20e3"));
                    await boardMsg.AddReactionAsync(new Emoji("7\u20e3"));

                    string gameBoard = RunningGames.BuildGameBoard(boardList);

                    e = new EmbedBuilder()
                    {
                        Title = $"{Context.User.Username}'s Turn",
                        Description = gameBoard,
                        Author = new EmbedAuthorBuilder()
                        {
                            Name = $"Connect Four >>> {Context.User.Username} vs {opponent.Username}",
                        },
                        Color = LunaUtil.lightBlue
                    };

                    await boardMsg.ModifyAsync(x => x.Embed = (Embed)e);

                    object[] info = { Context.User.Id, opponent.Id, boardList, 1 };
                    RunningGames.ConnectFourGames.Add(boardMsg.Id, info);
                }
                else
                {
                    await LunaUtil.CommandFail(Context.Message);
                    await ReplyAsync("You're already playing a game with someone!");
                }
            }

            [Command("stop")]
            [Summary("Stop all connect four games or just ones with specified user")]
            public async Task Stop(
                [Summary("Opponent to stop games with")] IGuildUser opponent = null)
            {
                if (opponent == null)
                {
                    var games = RunningGames.ConnectFourGames.Where(x => (ulong)x.Value[0] == Context.User.Id || (ulong)x.Value[1] == Context.User.Id);

                    foreach (var g in games)
                        RunningGames.ConnectFourGames.Remove(g.Key);
                }
                else
                {
                    var games = RunningGames.ConnectFourGames.Where(x => ((ulong)x.Value[0] == Context.User.Id && (ulong)x.Value[1] == opponent.Id)
                                                                    || ((ulong)x.Value[1] == Context.User.Id && (ulong)x.Value[0] == opponent.Id));

                    foreach (var g in games)
                        RunningGames.ConnectFourGames.Remove(g.Key);
                }

                await LunaUtil.CommandSuccess(Context.Message);
            }

            [Command("rules")]
            [Summary("Display the rules of the game")]
            public async Task Rules()
            {
                EmbedBuilder rules = new EmbedBuilder()
                {
                    Title = "Connect Four Guide",
                    Description = "1. Wait for board to load" +
                        "\n2. Initiator of game goes first, click a number to drop token" +
                        "\n3. Switch turns every drop" +
                        "\n4. First to connect 4 tokens in a row (verticall, horizontally, diagonally) wins!",
                    Color = LunaUtil.lightBlue
                };

                await ReplyAsync("", embed: rules);
            }
        }

        [Group("HangMan")]
        public class HandMan : ModuleBase
        {
            [Command("start")]
            [Summary("Start a handman game with given word")]
            public async Task Start(
                [Summary("Word to guess"), Remainder] string word)
            {
                await Context.Message.DeleteAsync();
                await ReplyAsync($"{Context.User.Mention} is creating a hangman game...");


            }
        }

        [Command("rolldice")]
        [Summary("Roll a dice")]
        [Alias("diceroll")]
        public async Task RollDice()
        {
            Random rand = new Random();
            await ReplyAsync($"Dice Roll: {rand.Next(7) + 1}");
        }

        [Command("flipcoin")]
        [Summary("Flip a coin")]
        [Alias("coinflip")]
        public async Task FlipCoin()
        {
            Random rand = new Random();
            await ReplyAsync($"Coin Flip: {(rand.Next(2) == 0 ? "Heads" : "Tails")}");
        }

        [Command("choosecard")]
        [Summary("Choose a card from a standard deck")]
        public async Task ChooseCard()
        {
            Random rand = new Random();
            int card = rand.Next(52) + 1;
            string cardName = "";

            if (card >= 1 && card <= 13) // Black of clubs
            {
                if (card <= 9)
                    cardName = card == 0 ? "Ace of Clubs" : $"{card} of Clubs";
                else
                {
                    switch (card)
                    {
                        case 10:
                            cardName = "Jack of Clubs";
                            break;
                        case 11:
                            cardName = "Queen of Clubs";
                            break;
                        case 12:
                            cardName = "King of Clubs";
                            break;
                    }
                }
            }
            else if (card >= 14 && card <= 26) // Red of diamonds
            {
                if (card <= 22)
                    cardName = card == 12 ? "Ace of Clubs" : $"{card - 11} of Diamonds";
                else
                {
                    switch (card)
                    {
                        case 23:
                            cardName = "Jack of Clubs";
                            break;
                        case 24:
                            cardName = "Queen of Clubs";
                            break;
                        case 25:
                            cardName = "King of Clubs";
                            break;
                    }
                }
            }
            else if (card >= 27 && card <= 39) // Red of hearts
            {
                if (card <= 35)
                    cardName = card == 26 ? "Ace of Clubs" : $"{card - 24} of Hearts";
                else
                {
                    switch (card)
                    {
                        case 36:
                            cardName = "Jack of Clubs";
                            break;
                        case 37:
                            cardName = "Queen of Clubs";
                            break;
                        case 38:
                            cardName = "King of Clubs";
                            break;
                    }
                }
            }
            else if (card >= 40 && card <= 52) // Black of spades
            {
                if (card <= 48)
                    cardName = card == 39 ? "Ace of Clubs" : $"{card - 37} of Spades";
                else
                {
                    switch (card)
                    {
                        case 49:
                            cardName = "Jack of Clubs";
                            break;
                        case 50:
                            cardName = "Queen of Clubs";
                            break;
                        case 51:
                            cardName = "King of Clubs";
                            break;
                    }
                }
            }

            await ReplyAsync($"Chose a card: {cardName}");
        }
    }
}

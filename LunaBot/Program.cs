﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Discord.Addons.Interactive;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using System.Threading;
using System.Data;

namespace LunaBot
{
    public class Program
    {
        private DiscordSocketClient client;
        private CommandService commands;
        private IServiceProvider service;
        private LunaConfig lunaConfig;
        private List<ModuleInfo> moduleList = new List<ModuleInfo>();
        
        static void Main(string[] args) => 
            new Program().Start().GetAwaiter().GetResult();

        /// <summary>
        /// This starts everything
        /// </summary>
        /// <returns>
        /// Returns a task
        /// </returns>
        // "Main" method
        public async Task Start()
        {
            LunaConfig.EnsureExists();
            lunaConfig = LunaConfig.Load();

            // Initialize client
            client = new DiscordSocketClient();
            client.Log += Log;
            // Events
            client.UserJoined += UserJoined;
            client.UserLeft += UserLeft;
            client.ReactionAdded += ReactionAdded;
            
            commands = new CommandService();
            await InstallCommands();

            // Dependency injection
            service = ConfigureServices();
            service.GetRequiredService<DiscordSocketClient>();
            service.GetRequiredService<CommandService>();
            service.GetRequiredService<List<ModuleInfo>>();
            service.GetRequiredService<LunaConfig>();

            // Connect to discord
            await client.LoginAsync(TokenType.Bot, lunaConfig.Token);
            await client.StartAsync();
            client.Ready += ReadyEvent;

            await Task.Delay(-1); // Infinite delay
        }

        /// <summary>
        /// Hooks message received event and adds modules to program
        /// </summary>
        /// <returns></returns>
        // Hook message events and add modules
        public async Task InstallCommands()
        {
            client.MessageReceived += HandleCommand;
            await commands.AddModulesAsync(Assembly.GetEntryAssembly());
        }

        /// <summary>
        /// Handles message received event
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        // Handle command issue
        public async Task HandleCommand(SocketMessage msg)
        {
            var message = msg as SocketUserMessage;

            if (message == null) return;

            int argPos = 0;
            
            if (!(message.HasCharPrefix(lunaConfig.Prefix, ref argPos) || message.HasMentionPrefix(client.CurrentUser, ref argPos))) return;
            
            var context = new CommandContext(client, message);
            var result = await commands.ExecuteAsync(context, argPos, service);

            if (!result.IsSuccess) // Send error message to channel if unsuccessful
                await message.Channel.SendMessageAsync(result.ErrorReason);
        }

        /// <summary>
        /// Logs to console
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        // Log events
        private Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }

        /// <summary>
        /// Adds depedencies to service collection
        /// </summary>
        /// <returns></returns>
        // Depedencies
        private IServiceProvider ConfigureServices()
        {
            foreach (var module in commands.Modules)
            {
                if (module.Parent == null) // Is own page
                {
                    moduleList.Add(module);
                }
            }

            moduleList = moduleList.OrderBy(x => int.Parse(x.Remarks)).ToList();

            return new ServiceCollection()
                .AddSingleton(client)
                .AddSingleton(commands)
                .AddSingleton<InteractiveService>()
                .AddSingleton(moduleList)
                .AddSingleton<LunaConfig>()
                .BuildServiceProvider();
        }

        private static Timer timer;

        /// <summary>
        /// Starts timer
        /// </summary>
        private void StartTimer()
        {
            timer = new Timer(x => Execute(), null, 0, Timeout.Infinite);
        }

        /// <summary>
        /// Performs periodic checks on guilds
        /// </summary>
        private async void Execute()
        {
            foreach (IGuild guild in client.Guilds)
            {
                GuildConfig config = GuildConfig.Load(guild);

                LunaUtil.LogMsg(0, "Checking Guild", guild.Name);

                List<PrivateChannelModel> channelsToRemove = new List<PrivateChannelModel>();
                List<ModActionsModel> actionsToRemove = new List<ModActionsModel>();

                // Check private channels
                foreach (PrivateChannelModel privChannel in config.PrivateChannels)
                {
                    try
                    {
                        IVoiceChannel channel = await guild.GetVoiceChannelAsync(privChannel.ChannelID);
                        int userCount = channel.GetUsersAsync().Flatten().Result.Count();
                        LunaUtil.LogMsg(1, "Channel", channel.Name + " " + channel.Id);

                        if (DateTime.Now > privChannel.TimeSinceEmpy && userCount == 0)
                        {
                            config.PrivateChannels.Remove(privChannel);
                            channelsToRemove.Add(privChannel);
                            await channel.DeleteAsync();
                        }
                        else if (userCount > 0)
                        {
                            privChannel.TimeSinceEmpy = DateTime.Now.AddMinutes(10);
                        }
                    }
                    catch (Exception e)
                    {
                        LunaUtil.LogMsg(1, "Channel", "no longer exists, removing...");
                        channelsToRemove.Add(privChannel);
                    }
                }

                // Check mod actions
                foreach (ModActionsModel action in config.ModActions)
                {
                    try
                    {
                        LunaUtil.LogMsg(1, "Mod Action", action.ModOperation + " #" + action.ModActionID.ToString());

                        if (action.IssueTime.AddMinutes(action.Duration) < DateTime.Now && action.Duration != -1)
                        {
                            LunaUtil.UnAction(guild, action, action.UserID, action.ModOperation);
                            actionsToRemove.Add(action);
                        }
                    }
                    catch (Exception e)
                    {
                        LunaUtil.LogMsg(1, "Mod Action", "error on mod action, removing...");
                        actionsToRemove.Add(action);
                    }
                }

                config.ModActions.RemoveAll(x => actionsToRemove.Any(y => y.ModActionID == x.ModActionID));
                config.PrivateChannels.RemoveAll(x => channelsToRemove.Any(y => y.PrivateChannelID == x.PrivateChannelID));

                config.Save();
            }

            timer.Change(1000 * 60 * 5, Timeout.Infinite);
        }

        //-----------------------------Events-----------------------------//
        private async Task ReadyEvent()
        {
            IApplication bot = await client.GetApplicationInfoAsync();
            await client.CurrentUser.ModifyAsync(x =>
            {
                x.Username = bot.Name;
            });

            await client.SetGameAsync($"{lunaConfig.Prefix}help");

            foreach (SocketGuild guild in client.Guilds)
            {
                GuildConfig.EnsureExists(guild);
                GuildConfig config = GuildConfig.Load(guild);

                // Load commands
                foreach (CommandInfo cmd in commands.Commands)
                {
                    string cmdName = config.EnabledCommands.Where(x => x.ToLower().Equals(cmd.Name.ToLower())).FirstOrDefault();

                    if (cmdName == null)
                    {
                        config.EnabledCommands.Add(cmd.Name);
                    }
                }

                config.Save();

                LunaUtil.LogMsg(0, $"Guild : {guild.Name}", "connected");
            }

            StartTimer();
        }

        private async Task UserJoined(SocketGuildUser user)
        {
            SocketGuild guild = user.Guild;
            GuildConfig config = GuildConfig.Load(guild);

            IRole role = guild.GetRole(config.RoleOnJoin);
            await user.AddRoleAsync(role);

            if (config.Greeting != null)
            {
                SocketTextChannel channel = guild.GetChannel(config.DefaultChannel) as SocketTextChannel;
                string greetingMessage = config.Greeting;
                greetingMessage = greetingMessage.Replace("{USER}", user.Username);

                await channel.SendMessageAsync(greetingMessage);
            }
        }

        private async Task UserLeft(SocketGuildUser user)
        {
            SocketGuild guild = user.Guild;
            GuildConfig config = GuildConfig.Load(guild);

            if (config.Farewell != null)
            {
                SocketTextChannel channel = guild.GetChannel(config.DefaultChannel) as SocketTextChannel;
                string leaveMessage = config.Farewell;
                leaveMessage = leaveMessage.Replace("{USER}", user.Username);

                await channel.SendMessageAsync(leaveMessage);
            }
        }

        // TODO: Redo this entire thing
        private async Task ReactionAdded(Cacheable<IUserMessage, ulong> msg, ISocketMessageChannel channel, SocketReaction reaction)
        {
            IUserMessage m = channel.GetMessageAsync(msg.Id).Result as IUserMessage;

            if (!reaction.User.Value.IsBot && m != null)
            {
                // Connect Four Handler
                if (RunningGames.ConnectFourGames.ContainsKey(msg.Id))
                {
                    object[] val = null;
                    RunningGames.ConnectFourGames.TryGetValue(msg.Id, out val);

                    if ((reaction.UserId == (ulong)val[0] && 1 == (int)val[3]) || (reaction.UserId == (ulong)val[1] && 2 == (int)val[3])) // Is a player in play
                    {
                        int turn = (int)val[3];
                        string token = (int)(val[3]) == 1 ? "\U0001f535" : "\U0001f534";
                        int column = int.Parse(reaction.Emote.ToString().Substring(0, 1)) - 1;

                        var p1 = channel.GetUserAsync((ulong)val[0]).Result as SocketUser;
                        var p2 = channel.GetUserAsync((ulong)val[1]).Result as SocketUser;

                        string[,] boardList = val[2] as string[,];
                        int xCoord = 0;
                        int yCoord = 0;
                        Boolean hasSpot = false;

                        for (int r = boardList.GetUpperBound(0); r >= 0; r--)
                        {
                            if (boardList[r, column].Contains(" "))
                            {
                                xCoord = column;
                                yCoord = r;
                                boardList[r, column] = token;
                                hasSpot = true;
                                break;
                            }
                        }

                        if (hasSpot)
                        {
                            RunningGames.ConnectFourGames[msg.Id][2] = boardList;
                            EmbedBuilder e = null;

                            if (CheckForWin(boardList, token, xCoord, yCoord) ?? false)
                            {
                                // Build win message
                                e = new EmbedBuilder()
                                {
                                    Title = ">>> " + (turn == 1 ? p1.Username : p2.Username) + " Wins! <<<",
                                    Description = RunningGames.BuildGameBoard(boardList),
                                    Author = new EmbedAuthorBuilder()
                                    {
                                        Name = $"Connect Four >>> {p1.Username} vs {p2.Username}",
                                    },
                                    ThumbnailUrl = (turn == 1 ? p1.GetAvatarUrl() : p2.GetAvatarUrl()),
                                    Color = LunaUtil.lightBlue
                                };

                                RunningGames.ConnectFourGames.Remove(msg.Id);
                            }
                            else if (!CheckForWin(boardList, token, xCoord, yCoord) ?? false)
                            {
                                RunningGames.ConnectFourGames[msg.Id][3] = turn == 1 ? 2 : 1;

                                // Build reg message
                                e = new EmbedBuilder()
                                {
                                    Title = (turn == 1 ? p2.Username : p1.Username) + "'s Turn",
                                    Description = RunningGames.BuildGameBoard(boardList),
                                    Author = new EmbedAuthorBuilder()
                                    {
                                        Name = $"Connect Four >>> {p1.Username} vs {p2.Username}",
                                    },
                                    Color = LunaUtil.lightBlue
                                };
                            }
                            else if (CheckForWin(boardList, token, xCoord, yCoord) == null)
                            {
                                // Build win message
                                e = new EmbedBuilder()
                                {
                                    Title = ">>> Draw! <<<",
                                    Description = RunningGames.BuildGameBoard(boardList),
                                    Author = new EmbedAuthorBuilder()
                                    {
                                        Name = $"Connect Four >>> {p1.Username} vs {p2.Username}",
                                    },
                                    Color = LunaUtil.lightBlue
                                };
                            }

                            var message = channel.GetMessageAsync(msg.Id).Result as IUserMessage;
                            await message.ModifyAsync(x => x.Embed = (Embed)e);
                        }
                    }

                    await m.RemoveReactionAsync(reaction.Emote, reaction.User.Value);
                }
                else if (m.Embeds.Count > 0 && m.Embeds.First().Footer.ToString().StartsWith("!help") && m.MentionedUserIds.Contains(reaction.UserId))
                {
                    if (int.TryParse(reaction.Emote.ToString().Substring(0, 1), out int page))
                    {
                        if (page >= 1 && page <= moduleList.Count)
                        {
                            Embed e1 = LunaUtil.GetHelpPage(page, service.GetService<List<ModuleInfo>>(), new CommandContext(client, m));
                            Embed e2 = new EmbedBuilder()
                            {
                                Title = "\u200B",
                                Description = "\u200B",
                                Color = LunaUtil.offWhite
                            }.Build();

                            await m.ModifyAsync(x => x.Embed = e2);
                            await m.ModifyAsync(x => x.Embed = e1);

                            await m.RemoveReactionAsync(reaction.Emote, reaction.User.Value);
                        }
                    }
                }
            }
        }

        public Boolean? CheckForWin(string[,] boardList, string token, int xCoord, int yCoord)
        {
            int total = 0;
            // Check vertical |
            for (int y = 0; y <= boardList.GetUpperBound(0); y++)
            {
                if (boardList[y, xCoord].Contains(token))
                {
                    total++;

                    if (total >= 4)
                        return true;
                }
                else
                {
                    total = 0;
                }
            }

            total = 0;
            // Check horizontal -
            for (int x = 0; x <= boardList.GetUpperBound(1); x++)
            {
                if (boardList[yCoord, x].Contains(token))
                {
                    total++;

                    if (total >= 4)
                        return true;
                }
                else
                {
                    total = 0;
                }
            }

            int total1 = 1;
            int total2 = 1;

            // Check diagonal / \
            for (int i = 1; i <= 3; i++)
            {
                try
                {
                    if (boardList[yCoord - i, xCoord + i].Contains(token)) // Pos right
                    {
                        total1++;

                        if (total1 >= 4)
                            return true;
                    }
                }
                catch { }

                try
                {
                    if (boardList[yCoord + i, xCoord - i].Contains(token)) // Pos left
                    {
                        total1++;
                        Console.WriteLine(total1);
                        if (total1 >= 4)
                            return true;
                    }
                }
                catch { }

                try
                {
                    if (boardList[yCoord + i, xCoord + i].Contains(token)) // Neg right
                    {
                        total2++;

                        if (total2 >= 4)
                            return true;
                    }
                }
                catch { }

                try
                {
                    if (boardList[yCoord - i, xCoord - i].Contains(token)) // Neg left
                    {
                        total2++;

                        if (total2 >= 4)
                            return true;
                    }
                }
                catch { }
            }

            total = 0;
            // Check for tie
            for (int x = 0; x <= boardList.GetUpperBound(1); x++)
            {
                if (!boardList[0, x].Contains(" "))
                {
                    total++;

                    if (total >= boardList.GetUpperBound(1))
                        return null;
                }
            }

            return false;
        }
    }
}

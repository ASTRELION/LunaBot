﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;

namespace LunaBot
{
    public class LunaUtil
    {
        public static Color lightBlue = new Color(0, 200, 255);
        public static Color offWhite = new Color(210, 210, 210);
        public static Color red = new Color(255, 0, 0);
        public static Color yellow = new Color(255, 255, 0);
        public static Color green = new Color(0, 255, 0);
        public static Color black = new Color(1, 1, 1);

        public enum modOperations { Kick, Mute, TextMute, Ban, SoftBan }

        private LunaUtil() { } // Prevent instantiation

        public static void LogMsg(int level, string subject, string msg = "")
        {
            string space = "".PadLeft(level * 3);
            Console.WriteLine($"{DateTime.Now.ToString("HH:mm:ss")}{space} {subject}\t{msg}");
        }

        public static async Task CommandSuccess(IUserMessage msg) => await msg.AddReactionAsync(new Emoji("\u2705"));

        public static async Task CommandFail(IUserMessage msg) => await msg.AddReactionAsync(new Emoji("\u274C"));

        public static Embed GetHelpPage(int pageNo, List<ModuleInfo> moduleList, CommandContext context)
        {
            ModuleInfo page;
            
            if (pageNo >= 1 && pageNo <= moduleList.Count)
            {
                page = moduleList[pageNo - 1];
                List<CommandInfo> parentsCommands = page.Commands.OrderBy(x => x.Aliases[0]).Where(x => x.CheckPreconditionsAsync(context).Result.IsSuccess).ToList();
                List<ModuleInfo> subModules = page.Submodules.OrderBy(x => x.Name).ToList();

                EmbedBuilder embedPage = new EmbedBuilder()
                {
                    Title = $"{page.Name} Help",
                    Description = "\u200B",
                    Footer = new EmbedFooterBuilder()
                    {
                        Text = $"!help | Page {pageNo} / {moduleList.Count}"
                    },
                    Color = offWhite
                };

                foreach (CommandInfo com in parentsCommands)
                {
                    embedPage.Description += $"**{GetCommandSyntax(com)}**";

                    if (com.Summary.Length > 0)
                    {
                        embedPage.Description += $": {com.Summary} ";
                    }

                    embedPage.Description += "\n";
                }

                foreach (ModuleInfo subMod in subModules)
                {
                    EmbedFieldBuilder subField = new EmbedFieldBuilder()
                    {
                        Name = subMod.Name,
                        Value = "\u200B"
                    };

                    BuildField(subMod);

                    void BuildField(ModuleInfo m)
                    {
                        foreach (CommandInfo subCom in m.Commands.Where(x => x.CheckPreconditionsAsync(context).Result.IsSuccess))
                        {
                            subField.Value += $"**{GetCommandSyntax(subCom)}**";

                            if (subCom.Summary.Length > 0)
                            {
                                subField.Value += $": {subCom.Summary} ";
                            }

                            subField.Value += "\n";
                        }

                        foreach (ModuleInfo subSub in m.Submodules)
                        {
                            BuildField(subSub);
                        }
                    }

                    if (!subField.Value.Equals("\u200B"))
                        embedPage.AddField(subField);
                }

                return embedPage.Build();
            }

            return null;
        }

        public static string GetCommandSyntax(CommandInfo command)
        {
            string commandSyntax = $"!{command.Aliases[0]} ";

            if (command.Parameters.Count > 0)
            {
                foreach (ParameterInfo param in command.Parameters)
                {
                    if (param.IsOptional)
                    {
                        commandSyntax += $"[{param.Name}] ";
                    }
                    else
                    {
                        commandSyntax += $"<{param.Name}> ";
                    }
                }
            }

            return commandSyntax;
        }

        public static Embed ModLog(ModActionsModel action, IGuildUser user, IGuildUser moderator, string reason = "")
        {
            EmbedBuilder logMessage = new EmbedBuilder()
            {
                Title = $"Case #{action.ModActionID.ToString("0000")} | Action: {(modOperations)action.ModOperation}",
                Color = action.Duration == -1 ? red : yellow,
            };

            EmbedFieldBuilder userField = new EmbedFieldBuilder()
            {
                Name = "User",
                Value = $"{user.Username}#{user.Discriminator}" +
                        $"\n{user.Id}",
                IsInline = true
            };

            EmbedFieldBuilder modField = new EmbedFieldBuilder()
            {
                Name = "Moderator",
                Value = $"{moderator.Username}#{moderator.Discriminator}" +
                        $"\n{moderator.Id}",
                IsInline = true
            };

            EmbedFieldBuilder reasonField = new EmbedFieldBuilder()
            {
                Name = "Reason",
                Value = $"{(reason == "" ? "*No reason given*" : reason)}",
                IsInline = true
            };

            EmbedFieldBuilder issueTime = new EmbedFieldBuilder()
            {
                Name = "Issued At",
                Value = $"{DateTime.Now}",
                IsInline = true
            };

            EmbedFieldBuilder expirationField = new EmbedFieldBuilder()
            {
                Name = "Expires at",
                Value = $"{(action.Duration == 0 ? "Immediate" : DateTime.Now.AddMinutes(action.Duration).ToString())}",
                IsInline = true
            };

            logMessage.AddField(userField);
            logMessage.AddField(modField);
            logMessage.AddField(reasonField);
            logMessage.AddField(issueTime);
            logMessage.AddField(expirationField);

            IDMChannel dm = user.GetOrCreateDMChannelAsync().Result;
            dm.SendMessageAsync("A moderator has performed a command on you", embed: logMessage.Build());

            return logMessage.Build();
        }

        public static async void UnAction(IGuild guild, ModActionsModel action, ulong userID, int modOperation)
        {
            IGuildUser user = await guild.GetUserAsync(userID);

            switch (modOperation)
            {
                case (int)modOperations.Mute:
                    await user.ModifyAsync(x => x.Mute = false);
                    break;

                case (int)modOperations.TextMute:
                    foreach (ulong channelID in action.Channels)
                    {
                        ITextChannel channel = await guild.GetTextChannelAsync(channelID);
                        await channel.RemovePermissionOverwriteAsync(user);
                    }

                    break;

                case (int)modOperations.Ban:
                    await guild.RemoveBanAsync(user);
                    break;
            }
        }
    }
}

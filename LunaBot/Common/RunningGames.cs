﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunaBot
{
    public class RunningGames
    {
        public static Dictionary<ulong, object[]> ConnectFourGames { get; set; } = new Dictionary<ulong, object[]>(); // [messageID[player1,player2,gameboard(array), turn]]
        public static Dictionary<ulong, List<string>> HangManGames { get; set; } = new Dictionary<ulong, List<string>>(); // [messageID[word,lettersToGuess]]

        public static string BuildGameBoard(string[,] boardList)
        {
            string gameBoard = "\u200B\n";
            string padSpace = "        ";
            string line = $"\n\u200B{padSpace}**\u2015\u2015\u2015\u2015\u2015\u2015\u2015\u2015\u2015\u2015\u2015\u2015\u2015\u2015\u2015\u2015\u2015\u2015\u2015\u2015\u2015\u2015\u2015\u2015**\n";

            // Generate gameboard
            for (int r = 0; r <= boardList.GetUpperBound(0); r++)
            {
                for (int c = 0; c <= boardList.GetUpperBound(1); c++)
                {
                    if (c == 0)
                        gameBoard += "\u200B" + padSpace;

                    gameBoard += $"|   {boardList[r, c]}   ";

                    if (c == boardList.GetUpperBound(1))
                        gameBoard += "|";

                    if (c == boardList.GetUpperBound(1))
                        gameBoard += line;
                }
            }

            return gameBoard;
        }
    }
}

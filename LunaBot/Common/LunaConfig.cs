﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Discord;
using System.IO;
using System.Xml.Serialization;

namespace LunaBot
{
    public class LunaConfig
    {
        private static string file = Path.Combine(AppContext.BaseDirectory, "Data\\_LunaConfig.xml");

        public string Token { get; set; }
        public char Prefix { get; set; }
        public int[] EmbedColor = {220, 220, 220};

        public static void EnsureExists()
        {
            string file = Path.Combine(AppContext.BaseDirectory, "Data\\_LunaConfig.xml");

            if (!File.Exists(file))
            {
                LunaConfig config = new LunaConfig();

                Console.Write("Enter bot token: ");
                string token = Console.ReadLine();

                Console.Write("Enter command prefix: ");
                string prefix = Console.ReadLine();

                while (prefix.Length != 1)
                {
                    Console.WriteLine("[ERROR: Prefix must be 1 character]\nEnter command prefix: ");
                    prefix = Console.ReadLine();
                }

                config.Token = token;
                config.Prefix = prefix.ToCharArray().First();
                config.Save();
            }
        }

        public void Save()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(LunaConfig));
            TextWriter writer = new StreamWriter(file);
            serializer.Serialize(writer, this);
            writer.Close();
        }

        public void Delete()
        {
            File.Delete(file);
        }

        public static LunaConfig Load()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(LunaConfig));
            TextReader reader = new StreamReader(file);
            object obj = serializer.Deserialize(reader);
            LunaConfig config = obj as LunaConfig;
            reader.Close();
            return config;
        }
    }
}

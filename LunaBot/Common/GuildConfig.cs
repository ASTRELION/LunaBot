﻿using System;
using System.Collections.Generic;
using System.Linq;

using Discord;
using System.IO;
using System.Xml.Serialization;

namespace LunaBot
{
    public class GuildConfig
    {
        // Guild actions and events
        public long ModActionID { get; set; }
        public long PrivateChannelID { get; set; }

        // Guild info
        public ulong GuildID { get; set; }
        public ulong RoleOnJoin { get; set; }
        public ulong DefaultChannel { get; set; }
        public ulong LogChannel { get; set; }

        // Guild customization
        public string GuildName { get; set; }
        public string Greeting { get; set; }
        public string Farewell { get; set; }

        // Guild settings
        public int LogLevel { get; set; }

        // Guild data
        public List<PrivateChannelModel> PrivateChannels = new List<PrivateChannelModel>();
        public List<ModActionsModel> ModActions = new List<ModActionsModel>();
        public List<string> EnabledCommands = new List<string>();

        /// <summary>
        /// Ensures guild data file exists. Creates a new one and populates it with default data if no file exists, or updated existing file.
        /// </summary>
        /// <param name="guild">Guild to check</param>
        public static void EnsureExists(IGuild guild)
        {
            string file = $"{AppContext.BaseDirectory}Data\\Guild{guild.Id}.xml";

            if (!File.Exists(file))
            {
                GuildConfig config = new GuildConfig();
                config.GuildID = guild.Id;
                config.GuildName = guild.Name;
                config.RoleOnJoin = guild.EveryoneRole.Id;
                config.DefaultChannel = guild.DefaultChannelId;
                config.LogChannel = guild.DefaultChannelId;
                config.LogLevel = 0;
                config.ModActionID = 1;
                config.PrivateChannelID = 1;
                
                config.Save();
            }
            else
            {
                GuildConfig config = Load(guild);
                config.GuildName = guild.Name; // Update guild name on every load
                config.Save();
            }
        }

        /// <summary>
        /// Saves a guild config (writes file)
        /// </summary>
        public void Save()
        {
            string file = $"{AppContext.BaseDirectory}Data\\Guild{GuildID}.xml";

            XmlSerializer serializer = new XmlSerializer(typeof(GuildConfig));
            TextWriter writer = new StreamWriter(file);
            serializer.Serialize(writer, this);
            writer.Close();
        }

        /// <summary>
        /// Deletes a guild config (deletes file)
        /// </summary>
        public void Delete()
        {
            string file = $"{AppContext.BaseDirectory}Data\\Guild{GuildID}.xml";
            File.Delete(file);
        }

        /// <summary>
        /// Loads given guild (reads file)
        /// </summary>
        /// <param name="guild">Guild to load</param>
        /// <returns>Guild configuration of given guild</returns>
        public static GuildConfig Load(IGuild guild)
        {
            string file = $"{AppContext.BaseDirectory}Data\\Guild{guild.Id}.xml";

            XmlSerializer serializer = new XmlSerializer(typeof(GuildConfig));
            TextReader reader = new StreamReader(file);
            object obj = serializer.Deserialize(reader);
            GuildConfig config = obj as GuildConfig;
            reader.Close();
            return config;
        }
    }
}

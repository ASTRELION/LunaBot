﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Discord;

namespace LunaBot
{
    public class ModActionsModel
    {
        public long ModActionID { get; set; }
        public ulong UserID { get; set; }
        public int ModOperation { get; set; }
        public DateTime IssueTime { get; set; }
        public int Duration { get; set; }
        public List<ulong> Channels = new List<ulong>();
    }
}

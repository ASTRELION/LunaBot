﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunaBot
{
    public class PrivateChannelModel
    {
        public long PrivateChannelID { get; set; }
        public ulong ChannelID { get; set; }
        public ulong OwnerID { get; set; }
        public DateTime TimeSinceEmpy { get; set; }
    }
}
